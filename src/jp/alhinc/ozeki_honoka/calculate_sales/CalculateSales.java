package jp.alhinc.ozeki_honoka.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		//コマンドライン引数が渡されてなかったらエラー出力。
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}

		//支店コードと支店名を関連付けるマップ
		Map<String, String> branchNames = new HashMap<>();
		//支店コードと売上金額を関連付けるマップ
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を関連付けるマップ
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと金額を関連付けるマップ
		Map<String, Long> commoditySales = new HashMap<>();

		//支店定義ファイルの読み込み
		if(!inputNames(args[0], "branch.lst", "^\\d{3}", "支店定義ファイル", branchNames, branchSales)){
			return;
		}



		//商品定義ファイルの読み込み
		if(!inputNames(args[0], "commodity.lst","^[A-Za-z0-9]{8}",  "商品定義ファイル", commodityNames, commoditySales)){
			return;
		}



		//売上ファイルの読み込みを行う。とりあえず全部引っ張ってくる。
		File[] files = new File(args[0]).listFiles();
		//正規表現で売り上げファイルだけとってきて、それだけを格納するリストを作成する。
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			if(files[i].isFile() && fileName.matches("^\\d{8}\\.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//rcdファイル(売上ファイル)が連番になっているか確認
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int later = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((later - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません。");
				return;

			}
		}


		//rcdファイルだけ格納されているリストからテキスト内容(支店コード、商品コード、売上額)を抽出。
		BufferedReader brBranchSale = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader frBranchSale = new FileReader(rcdFiles.get(i));
				brBranchSale = new BufferedReader(frBranchSale);

				//1行目の支店コード2行目の商品コード、3行目の金額を配列に格納
				String line;
				ArrayList<String> branchSalesItems = new ArrayList<String>();
				while((line = brBranchSale.readLine()) != null) {
					branchSalesItems.add(line);
				}

				//売上ファイルが3行になっているかの確認。違ったらエラー出力。
				if(branchSalesItems.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}
				//売上ファイルの中に入っている支店コード、または商品コードが存在していなかった場合、エラーを出力
				if(!branchNames.containsKey(branchSalesItems.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				if(!commodityNames.containsKey(branchSalesItems.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

				//支店コードと売上金額マップから今回処理している売り上げファイルの支店名の合計金額を取り出してきて
				//今回処理している売り上げファイルの金額と足し合わせる。(支店ごとの合計)
				//かつ、商品コードの売上についても上記と同様のことを行う。
				//足し合わせる前に、売上ファイルに入っている金額が数字なのか確認する。
				if(!branchSalesItems.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long fileSale = Long.parseLong(branchSalesItems.get(2));
				long branchSaleAmount = branchSales.get(branchSalesItems.get(0)) + fileSale;
				long commoditySaleAmount = commoditySales.get(branchSalesItems.get(1)) + fileSale;

				//支店ごとの合計金額、商品ごとの合計金額いずれも
				//10桁を超えてしまった(11桁から)場合エラーを出力
				if(branchSaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				if(commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//支店コードごとの売り上げと商品コードごとの売り上げを関連付けるマップの値を書き換える。
				branchSales.put(branchSalesItems.get(0), branchSaleAmount);
				commoditySales.put(branchSalesItems.get(1), commoditySaleAmount);

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(brBranchSale != null) {
					try {
						brBranchSale.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}



		//支店別集計ファイルに出力する
		if(!outputSales(args[0], "branch.out", branchNames, branchSales)){
			return;
		}



		//商品別集計ファイルに出力する。
		if(!outputSales(args[0], "commodity.out", commodityNames, commoditySales)){
			return;
		}



	}

	//各定義ファイル読み込みのメソッド化
	private static boolean inputNames(String args, String pass, String regularExpressions, String fileName,
			Map<String, String> names, Map<String, Long> sales) {
		BufferedReader br= null;
		try {
			File file = new File(args, pass);
			//各定義ファイルが存在しているかの確認
			if(!file.exists()) {
				System.out.print(fileName + "が存在しません。");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				//各定義ファイルのフォーマットがあっているか確認
				if(items.length != 2 || !items[0].matches(regularExpressions) ) {
					System.out.println(fileName + "のフォーマットが不正です。");
					return false;
				}

				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}return true;
	}

	//集計ファイル出力のメソッド化
	private static boolean outputSales(String args, String pass, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;
		try {
			File file = new File(args, pass);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key) + "\n");
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		} return true;
	}
}
